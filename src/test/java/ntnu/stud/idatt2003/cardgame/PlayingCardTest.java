package ntnu.stud.idatt2003.cardgame;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/*

 */
class PlayingCardTest {

  @Test
  void testPlayingCardInitializationValid() {
    assertDoesNotThrow(() -> new PlayingCard('S', 1), "Valid card initialization should not throw");
  }

  @Test
  void testPlayingCardInitializationInvalidSuit() {
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard('X', 1), "Invalid suit should throw IllegalArgumentException");
  }

  @Test
  void testPlayingCardInitializationInvalidFace() {
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard('S', 0), "Invalid face should throw IllegalArgumentException");
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard('S', 14), "Invalid face should throw IllegalArgumentException");
  }

  @Test
  void testGetAsString() {
    PlayingCard card = new PlayingCard('H', 1);
    assertEquals("♥1", card.getAsString(), "getAsString should return the correct string representation");
  }

  @Test
  void testEqualsSameCard() {
    PlayingCard card1 = new PlayingCard('H', 1);
    PlayingCard card2 = new PlayingCard('H', 1);
    assertEquals(card1, card2, "Two cards with the same suit and face should be equal");
  }

  @Test
  void testEqualsDifferentCard() {
    PlayingCard card1 = new PlayingCard('H', 1);
    PlayingCard card2 = new PlayingCard('S', 1);
    assertNotEquals(card1, card2, "Two cards with different suits should not be equal");
  }
}
