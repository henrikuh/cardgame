package ntnu.stud.idatt2003.cardgame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Represents a deck of 52 playing cards, spanning suits (Spades, Hearts, Diamonds, Clubs)
 * and ranks (Ace to King).
 * <p>
 * This class allows for dealing hands of cards, where each card is an instance
 * of {@link PlayingCard}.
 * </p>
 *
 * @author Henrik Hausberg
 * @version 2021-03-19
 */

public class DeckOfCards {
  private final List<PlayingCard> cards;

  /**
   * Initializes a new deck of cards in standard order: Spades, Hearts, Diamonds, and Clubs,
   * each ranging from Ace to King.
   */
  public DeckOfCards() {
    char[] suits = {'S', 'H', 'D', 'C'};
    cards = IntStream.range(0, suits.length * 13)
        .mapToObj(i -> new PlayingCard(suits[i / 13], 1 + i % 13))
        .collect(Collectors.toList());
  }

  /**
   * Deals a specified number of cards from the deck, randomly. The method shuffles the deck
   * and then selects the first {@code n} cards.
   *
   * @param n The number of cards to deal, must be between 1 and 52 inclusive.
   * @return An array of {@code PlayingCard}, representing the dealt hand.
   * @throws IllegalArgumentException If the requested number of cards is not within the valid range.
   */
  public PlayingCard[] giveHand(int n) {
    if (n < 1 || n > 52) {
      throw new IllegalArgumentException("Number of cards to deal must be between 1 and 52.");
    }
    Collections.shuffle(cards);
    return cards.stream()
        .limit(n)
        .toArray(PlayingCard[]::new);
  }
}
