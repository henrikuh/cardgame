package ntnu.stud.idatt2003.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {
  private DeckOfCards deck;

  @BeforeEach
  void setUp() {
    deck = new DeckOfCards();
  }

  @Test
  void testGiveHandValidNumberOfCards() {
    PlayingCard[] hand = deck.giveHand(5);
    assertEquals(5, hand.length, "Hand should have 5 cards");
  }

  @Test
  void testGiveHandInvalidNumberOfCards() {
    assertThrows(IllegalArgumentException.class, () -> deck.giveHand(0), "Should throw IllegalArgumentException for invalid number of cards");
    assertThrows(IllegalArgumentException.class, () -> deck.giveHand(53), "Should throw IllegalArgumentException for invalid number of cards");
  }

  @Test
  void testGiveHandUniqueCards() {
    PlayingCard[] hand = deck.giveHand(5);
    long uniqueCards = Arrays.stream(hand).distinct().count();
    assertEquals(hand.length, uniqueCards, "All cards in the hand should be unique");
  }
}
