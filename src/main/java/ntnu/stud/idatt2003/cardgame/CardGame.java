package ntnu.stud.idatt2003.cardgame;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static javafx.geometry.Pos.*;

/**
 * A JavaFX application for a card game. It displays a hand of cards, allows dealing a new hand,
 * and checks the current hand for certain properties (like flushes or specific cards).
 * The UI includes buttons for dealing a new hand and checking the hand, along with displays
 * for the hand itself and the results of checks.
 */
public class CardGame extends Application {
    DeckOfCards deck = new DeckOfCards();
    PlayingCard[] hand;
    HBox handBox = new HBox(10);
    VBox resultBox = new VBox(10);
    VBox heartCards = new VBox(10);
    private static final int HAND_SIZE = 5;

  @Override
  public void start(Stage stage) {

    hand = deck.giveHand(HAND_SIZE);
    updateHandDisplay();
    checkHand();

    HBox buttonBox = getButtons();

    handBox.setPadding(new Insets(10));
    handBox.setAlignment(CENTER);
    handBox.setStyle("-fx-background-color: #e0e0e0");

    resultBox.setPadding(new Insets(10));
    resultBox.setAlignment(CENTER_LEFT);
    resultBox.setStyle("-fx-font-size: 20");

    Label heartCardsLabel = new Label("Heart cards:");
    VBox heartCardsBox = new VBox(10, heartCardsLabel, heartCards);
    heartCardsBox.setPadding(new Insets(10));
    heartCardsBox.setAlignment(CENTER_RIGHT);
    heartCardsBox.setStyle("-fx-font-size: 20");


    BorderPane content = new BorderPane();
    content.setTop(buttonBox);
    content.setLeft(resultBox);
    content.setRight(heartCardsBox);
    content.setBottom(handBox);

    Scene scene = new Scene(content, 640, 640);
    stage.setTitle("Cardgame!");
    stage.setScene(scene);
    stage.show();
  }

  /**
   * Sets up the buttons for dealing a new hand and checking the hand.
   * @return An HBox containing the setup buttons.
   */
  private HBox getButtons() {
    HBox buttonBox = new HBox(10);
    buttonBox.setAlignment(CENTER);
    buttonBox.setPadding(new Insets(10));
    buttonBox.setStyle("-fx-background-color: #e0e0e0");

    Button btnDealHand = new Button("Deal hand");
    btnDealHand.setOnAction(e -> {
      hand = deck.giveHand(HAND_SIZE);
      updateHandDisplay();
    });

    Button btnCheckHand = new Button("Check hand");
    btnCheckHand.setOnAction(e -> {
      updateHeartCards();
      checkHand();
    });

    buttonBox.getChildren().addAll(btnDealHand, btnCheckHand);
    return buttonBox;
  }

  /**
   * Checks the current hand for specific conditions, such as flushes or the presence of
   * the queen of spades, and updates the UI with the results.
   */
  private void checkHand() {
    List<PlayingCard> cardsList = Arrays.asList(hand);

    // Check if the hand contains a flush using streams
    Map<Character, Long> suitCounts = cardsList.stream()
        .collect(Collectors.groupingBy(PlayingCard::getSuit, Collectors.counting()));
    boolean flush = suitCounts.values().stream().anyMatch(count -> count >= 5);

    // Check if the hand contains the queen of spades using streams
    boolean queenOfSpades = cardsList.stream().anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);

    // Calculate the sum of the faces of the cards using streams
    int sumOfFaces = cardsList.stream().mapToInt(PlayingCard::getFace).sum();

    // Display the results
    Label lblFlush = new Label(flush ? "Flush: Yes" : "Flush: No");
    Label lblQueenOfSpades = new Label(queenOfSpades ? "Queen of Spades: Yes" : "Queen of Spades: No");
    Label lblSumOfFaces = new Label("Sum of faces: " + sumOfFaces);

    resultBox.getChildren().clear();
    resultBox.getChildren().addAll(lblFlush, lblQueenOfSpades, lblSumOfFaces);
  }

  /**
   * Updates the display of the current hand in the UI.
   */
  private void updateHandDisplay() {
    handBox.getChildren().clear(); // Clear the previous hand display

    for (PlayingCard card : hand) {
      HBox cardBox = cardBox(card, 100);
      handBox.getChildren().add(cardBox);
    }
  }

  /**
   * Updates the display of cards of the heart suit from the current hand.
   */
  private void updateHeartCards() {
    heartCards.getChildren().clear(); // Clear the previous heart cards display

    for (PlayingCard card : hand) {
      if (card.getSuit() == 'H') {
        HBox cardBox = cardBox(card, 30);
        heartCards.getChildren().add(cardBox);
      }
    }
  }

  /**
   * Creates and styles an HBox representing a card for display in the UI.
   * @param card The PlayingCard to display.
   * @param size The base size for the displayed card.
   * @return An HBox styled to represent the provided PlayingCard.
   */
  private HBox cardBox(PlayingCard card, int size) {
    HBox cardBox = new HBox();
    cardBox.setAlignment(CENTER);
    cardBox.setMinSize(size, size * 1.5);
    cardBox.setStyle("-fx-background-color: white; -fx-border-color: black; -fx-border-width: 1; -fx-padding: 10 10 40 10;");

    Label label = new Label(card.getAsString());
    label.setFont(new Font(20));
    label.setStyle(String.format("-fx-font-weight: bold; -fx-text-fill: %s;", (card.getSuit() == 'H' || card.getSuit() == 'D')? "red" : "black"));

    cardBox.getChildren().add(label);
    return cardBox;
  }

  public static void main(String[] args) {
    launch();
  }
}
