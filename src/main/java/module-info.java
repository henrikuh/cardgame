module ntnu.stud.idatt2003.cardgame {
    requires javafx.controls;
    requires javafx.fxml;


    opens ntnu.stud.idatt2003.cardgame to javafx.fxml;
    exports ntnu.stud.idatt2003.cardgame;
}